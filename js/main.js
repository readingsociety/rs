Parse.initialize("QVa1QFMUJOcfut1d48Yfq3Wayfj4A4ceTfmw4fnv", "MrT5xtru6Sgn3TJpPeN9tfVru1FPRjVli0XoPM3r");
riot.mount("login");
riot.mount("signup");
riot.mount("loginstatus");
riot.mount("interestTag");
riot.mount('topten');
riot.mount('newten');
riot.mount("personal");
riot.mount("editInfo");
riot.mount('dailyRec');
riot.mount('learningStatus');
// interestTags related


riot.route('/posts',function(){
	riot.mount('#maincontent','postcontent');
});

riot.route('/posts/new',function(){
	riot.mount('#maincontent','post');
});

riot.route('/posts/*',function(id){
	riot.mount('#maincontent','postcontent',{ objectID:id });
});

riot.route.start(true);

$('#logo').click(function(){
	if(Parse.User.current()){
		window.location.href = 'personalPage.html';
	}else{
		window.location.href = 'index.html';
	}	
})

$('#tut-btn').click(function(){
	$('#tutorial').show();
	// $('#tutorial').scrollTop(-200);
})

$('#x').click(function(){
	$('#tutorial').hide();
})

// alert window on personal page
$('#alert-x-btn1').click(function(){
	$('#alert').addClass('hidden');
})

// alert window on profile page
$('#alert-x-btn2').click(function(){
	$('#alert').addClass('hidden');
	window.location.href = 'userprofile.html';
})
