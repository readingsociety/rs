<articleReview>
    <div id='list'>
        <h2>{ title }</h2>
        <h5>Difficulty: { difficulty }</h5>
        <h5>Posted: { time }</h5>
        <img src="{ picLink }">
        <p></p>
        <p id='preview'>{ content }</p>
        <button class="btn btn-default"><a href='/#posts/{ articleid }' style="color: black;">Read More</a></button>
        <hr>
    </div>
    <script>
    this.on('mount', function() {
        console.log('mount');
    });
    this.title = this.articleRew.get('title');
    this.difficulty = this.articleRew.get('difficulty');
    this.time = moment(this.articleRew.get("createdAt")).fromNow();
    this.picLink = this.articleRew.get('picLink');
    this.content = this.articleRew.get('content');
    this.articleid = this.articleRew.id;
    this.update();
    </script>
    <style scoped>     
    #preview {
        height: 40px;
        display: block;
        overflow: hidden;
    }
    </style>
</articleReview>
