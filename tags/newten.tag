<newten>
    <h2>Top 10 Newest Topic</h2>
    <ol>
        <li each={ post in newPosts}><a href='#posts/{ post.id }'>{ post.get("title") }</li>
    </ol>
    <script>
    var that = this;
    this.newPosts = undefined;
    var uquery = new Parse.Query('UserInfo');
    uquery.equalTo('user', Parse.User.current()).first().then(function(result) {
        var level = result.get('levelnum');
        return Parse.Promise.as(level);        
    }).then(function(level){
        var query = new Parse.Query("UserPost");
        query.equalTo("difficulty", level);    
        query.descending("createdAt").limit(10);
        query.find().then(function(result) {
            console.log(result);
            that.newPosts = result;
            that.update();
        });

    })
    
    </script>
</newten>
