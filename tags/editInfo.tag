<editInfo>
    <div>
        <div id="instruction2">
            <h1>Welcome to Reading Society, <span>{ reader }!</span></h1>
        </div>
        <div id="info2">
            <div class="container">
                <h2>Please fill out the form to help us know more about you.</h2>
                <!-- self-intro -->
                <form>
                    <h3>Self-Introduction:</h3>
                    <textarea id="selfContent" rows=8 cols=200 placeholder="Introduce Yourself here!"></textarea>
                    <h4><h3>Choose your picture.</h3><input id="myPic" type="file"></h4>
                </form>
                <!-- Interest -->
                <div id="myInterest">
                    <h3>Reading Interest:</h3>
                    <label>
                        <input type="checkbox" value="stem"> STEM </label>
                    <label>
                        <input type="checkbox" value="culture"> Culture </label>
                    <label>
                        <input type="checkbox" value="health"> Health </label>
                    <label>
                        <input type="checkbox" value="entertainment"> Entertainment </label>
                </div>
                <!-- English Level -->
                <div id="myLevel">
                    <h3>English Level:</h3>
                    <button class="btn btn-info btn-sm" id="testLevel" onclick={ testLevel }>Test Your Level</button>
                    <div class="hidden" id="testEng">
                        <div>
                            <embed src='https://learnenglishteens.britishcouncil.org/content' width="1000" height="800"></embed>
                        </div>
                        <button class="btn btn-success btn-sm" id="testLevelTwo" onclick={ testLevelTwo }>Close The Test</button>
                    </div>
                    <h3 style="color:red">Report your level down here.</h3>
                    <p> 1 = A1~A2 (beginner);
                        <br>2 = A2~B1 ;
                        <br>3 = B1~B2 ;
                        <br>4 = B2~C1 ;
                        <br>5 = C1~C2 (advanced)</p>
                    <label class="radio-inline">
                        <input type="radio" name="levelNum" value="1">1</label>
                    <label class="radio-inline">
                        <input type="radio" name="levelNum" value="2">2</label>
                    <label class="radio-inline">
                        <input type="radio" name="levelNum" value="3">3</label>
                    <label class="radio-inline">
                        <input type="radio" name="levelNum" value="4">4</label>
                    <label class="radio-inline">
                        <input type="radio" name="levelNum" value="5">5</label>
                    <p></p>
                </div>
                <button class="btn btn-warning" id="allSubmit" onclick={ allSubmit }>Submit</button>
            </div>
        </div>
    </div>
    <script>
    var that = this;
    this.reader = Parse.User.current().get("username");

    this.testLevel = function(event) {
        $("#testEng").removeClass("hidden")
    }
    this.testLevelTwo = function(event) {
        $("#testEng").addClass("hidden")
    }

    var UserInfo = Parse.Object.extend("UserInfo");
    that.myInfo = new UserInfo();
    that.myInfo.set("user", Parse.User.current());

    this.allSubmit = function(event) {
        // store self-intro
        var introContent = $("#selfContent").val();
        that.myInfo.set("selfIntro", introContent);
        //store interest
        var readingInterest = $('#myInterest input:checked').map(function() {
            return $(this).val();
        }).toArray();
        that.myInfo.set("interest", readingInterest);
        // store level
        var myEngLevel = $('[name="levelNum"]:checked').val();
        console.log(myEngLevel);
        if (myEngLevel) {
            var myLevelNum = parseInt(myEngLevel);
            // that.myInfo.set("level", myEngLevel);
            that.myInfo.set("levelnum", myLevelNum);
            that.myInfo.set("records", []);
            that.myInfo.set("time", 0);

            //store file
            var fileElement = $("#myPic")[0];
            var filePath = $("#myPic").val();
            var fileName = filePath.split("\\").pop()

            if (fileElement.files.length > 0) {
                var file = fileElement.files[0];
                var newFile = new Parse.File(fileName, file);
                newFile.save({
                    success: function() {
                        //
                    },
                    error: function(error) {
                        alert("File Save Error:" + error.message)
                    }
                }).then(function(file) {
                    that.myInfo.set("file", file);
                    that.myInfo.save({
                        success: function() {
                            console.log("info saved!");
                            $('#alert').removeClass('hidden');
                            // window.location.href = 'userprofile.html';
                        },
                        error: function(error) {
                            alert("Error:" + error.message);
                        }
                    })

                })

            } else {
                that.myInfo.save({
                    success: function() {
                        alert("info saved!But we recommend you upload your picture!");
                        window.location.href = 'userprofile.html';
                    },
                    error: function(error) {
                        alert("Error:" + error.message);
                    }
                })
            }
        }else{
            alert("Please set your English level.")
        }
    }
    </script>
    <style scoped>
    span {
        display: inline-block;
    }
    
    span:first-letter {
        text-transform: uppercase;
    }
    
    #instruction2 {
        height: 40px;
        text-align: center;
        background-color: rgba(104, 65, 18, .7);
        color: white;
        width: 570px;
        border-radius: 5px;
    }
    
    #instruction2 h1 {
        font-weight: 300;
        font-size: 20px;
        padding-top: 8px;
    }
    
    #info2 h2 {
        font-weight: 500;
        font-size: 20px;
    }
    
    #info2 textarea {
        width: 375px;
        height: 100px;
    }
    
    #info2 h3 {
        font-weight: 500;
        outline-width: 18px;
        font-size: 18px;
    }
    
    #info2 h3 span,
    #info2 h4 {
        font-weight: 300;
        font-size: 16px;
    }
    </style>
</editInfo>
