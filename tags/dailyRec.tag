<dailyRec>
    <div id="today">
        <h2>Here's today's reading for you. </h2>
    </div>
    <div id="info">
        <h2>{ title }</h2>
        <h5>Author: { author }</h5>
        <h5>Posted: { time }</h5>
        <h5>Category: { postTags.toString() }</h5>
        <h5>Difficulty: { difficulty }</h5>
        <img src="{ picLink }">
        <p></p>
        <p each={ part in content.split(/\n/) }>{ part }</p>
    </div>
    <div id="feedback">
        <h2>Feedback</h2>
        <div>
            <form role="form">
                <div class="form-group">
                    <h3>Did you like today's recommendation?</h3>
                    <label class="radio-inline">
                        <input type="radio" name="feedback1" id="optionsRadios1" value="Yes"> Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="feedback1" id="optionsRadios2" value="No"> No
                    </label>
                </div>
                <div class="form-group">
                    <h3>How well did you understand this article?</h3>
                    <h5>( 1 = "Not at all.", 5 = "Very well.")</h5>
                    <label class="radio-inline">
                        <input type="radio" name="feedback2" id="optionsRadios1" value="1"> 1
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="feedback2" id="optionsRadios2" value="2"> 2
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="feedback2" id="optionsRadios3" value="3"> 3
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="feedback2" id="optionsRadios4" value="4"> 4
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="feedback2" id="optionsRadios5" value="5"> 5
                    </label>
                </div>
                <button class="btn btn-warning" onclick={ saveFeedback }>Submit</button>
            </form>
        </div>
    </div>
    <script>
    var that = this;
    // start the time
    this.on('mount', function() {
        that.timeSpent = 0;
        that.timer = setInterval(function() {
            that.timeSpent += 1;
        }, 1000);
    });
    that.title = "Waiting to be suprised..."
    that.reader = undefined;
    that.postId = undefined;
    that.likes = undefined;
    that.difficulty = undefined;
    var query = new Parse.Query('UserInfo');
    query.equalTo('user', Parse.User.current()).first().then(function(result) {
        that.reader = result;
        return Parse.Promise.as(that.reader);
    }).then(function(reader) {
        var level = reader.get('levelnum');
        that.difficulty = level;
        var interests = reader.get('interest');
        var interest = interests[Math.floor(Math.random() * interests.length)];
        console.log(interest);
        var readerId = reader.id;
        var query = new Parse.Query("UserPost");
        query.equalTo("difficulty", level);
        query.equalTo('interestTags', interest);
        query.notContainedIn('readBy', [readerId]);
        query.include('user');
        query.first().then(function(result) {
            // console.log(query.first());
            return Parse.Promise.as(result);
        }).then(function(result) {
            // console.log(result);
            var username = result.get('user').get("username");
            that.postId = result.id;
            that.title = result.get("title");
            that.author = username;
            that.time = moment(result.get("createdAt")).fromNow();
            that.picLink = result.get("picLink");
            that.content = result.get("content");
            that.postTags = result.get("interestTags");
            that.likes = result.get("likes");
            that.update();
        });
    });

    this.saveFeedback = function(event) {
        var UserPost = Parse.Object.extend('UserPost');
        var userPost = new UserPost();
        userPost.id = that.postId;
        var readerId = that.reader.id;
        userPost.addUnique('readBy', readerId);
        var valone = $('input[name=feedback1]:radio:checked').val();
        if (valone == "Yes") {
            if (that.likes) {
                that.likes += 1;
            } else {
                that.likes = 1;
            }
            userPost.set('likes', that.likes);
        }
        userPost.save();

        var valtwo = $('input[name=feedback2]:radio:checked').val();
        var confidence = Number(valtwo);
        var records = that.reader.get('records');
        var level = that.reader.get('levelnum');
        var totalTime = that.reader.get("time");
        console.log(totalTime);
        var i = records.length - 1;
        if (i >= 0) {
            if (confidence < 3 && records[i] < 3) {
                console.log('declined');
                $('#alert').removeClass('hidden');
                $('#alert-decline').removeClass('hidden');
                level -= 1;
                if (level < 1) {
                    level = 1;
                }
            } else if (confidence > 4 && records[i] > 3) {
                console.log('improved');
                $('#alert').removeClass('hidden');
                $('#alert-improve').removeClass('hidden');
                level += 1;
                if (level > 5) {
                    level = 5;
                }
            }
        }
        clearInterval(that.timer);
        console.log(that.timeSpent);
        totalTime = totalTime + that.timeSpent;
        console.log(totalTime);
        that.reader.add('records', confidence);
        that.reader.set('levelnum', level);
        that.reader.set('time', totalTime);
        that.reader.save();
        console.log('feedback received');
        $('#alert').removeClass('hidden');
        $('#alert-feedback').removeClass('hidden');
        that.timeSpent = 0;
    }
    </script>
    <style scoped>
    /* scoped styles suddenly don't work, so I also copy them to the css file */
    
    #today {
        height: 40px;
        background-color: rgba(115, 65, 18, .8);
        border-radius: 5px;
        text-align: center;
    }
    
    #today h2 {
        font-weight: 400;
        font-size: 20px;
        position: relative;
        padding-top: 8px;
        color: white;
    }
    
    #info h2 {
        font-weight: 500;
    }
    
    #info h5 {
        padding-top: 8px;
        font-weight: 400;
        font-size: 13px;
        line-height: 4px;
    }
    
    #feedback h2 {
        font-size: 24px;
    }
    
    #feedback h3 {
        font-weight: 400;
        font-size: 16px;
    }
    </style>
</dailyRec>
