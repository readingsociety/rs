
<post>
	<div class="container">
		<h1>Create Your Article!</h1>
		<form id="postForm" name="myForm">
	      <p><input id="postTitle" type="text" size= "58" placeholder="Enter Title"></p>
	      <p><textarea name='postContent' id="postContent" rows=10 cols=60 placeholder="Create your content here!"></textarea></p>
	      <button id="postSubmit" onclick={ createPost }>Submit</button>
	    </form>
		

		<!-- Display all posts -->
		<div id="listPost" each={ displayPost } >
			{username}
			<br>
			<span style="color:orange; font-size:25px">Title:{title}</span><br>
			{userArticle}
			<br>
			<button class="deletePost" onclick= { deletePost} >Delete</button>

			<!-- comment: users write comments on posts-->
			<div class="container">
				<div id="newComment">
					<!-- users' comments on others' posts are shown here -->
				</div>
				<form id="commentForm" name="myComment">
				  <p><textarea name='commentContent' id="commentContent" rows=10 cols=60 placeholder="Share your thoughts here!"></textarea></p>
				  <button id="commentSubmit" onclick={ createComment }>Submit</button>
				</form>
			</div>
		</div>


	<!-- JS -->
	<script>
		//Parse
		Parse.initialize("QVa1QFMUJOcfut1d48Yfq3Wayfj4A4ceTfmw4fnv", "MrT5xtru6Sgn3TJpPeN9tfVru1FPRjVli0XoPM3r");

		var Post = Parse.Object.extend("UserPost");
		this.displayPost = [];
		 
		var that = this
		function showAllPosts(event){
			
			var query = new Parse.Query("UserPost");
			query.ascending("createdAt").limit(2);
			query.find().then(function(allposts){
				that.displayPost = allposts;
				that.update();
			
			})
 
			// query.find({
			// 	success:function(allPosts){
			// 		that.displayPost = allposts;
			// 		that.update();
			// 		// alert("first")
			// 		}, 
			// 	error: function(error){

			// 		}
			// 	})
		}
		showAllPosts();


		this.createPost = function(event){
      		//store posts on Parse
			var newPost = new Post();
			var title = $("#postTitle").val();
      		var content = $("#postContent").val();
      		var user = Parse.User.current();

			newPost.set("title", title);
		    newPost.set("content", content);
		    newPost.set("user", user);

		    // define "output" of user for displaying username
      		var username = user.get("username");      				
      				
      		var output = "User:"+ " "+ username;

			var post = {
				username: output,
				userArticle: this.postContent.value,
				title: this.postTitle.value
			};
			this.displayPost.push(post); // display posts on web and push new post to the arrary displayPost

		    newPost.save({
		    	success:function(){
		    		// showAllPosts()
		    		alert("Article Created!")
		    	},
		    	error:function(error){
		    		alert("Error:" + error.message);
		    	}
		    })
		    var title = $("#postTitle").val("");
      		var content = $("#postContent").val("");
      		console.log(this.displayPost)
		}

		
		//Delete post
		this.deletePost = function(event){
			var targetPost = event.item;
			var index = this.displayPost.indexOf(targetPost);
			this.displayPost.splice(index,1);
		};
 		
 		// Comment on posts
 		this.createComment = function(event){
 			//store comments on Parse
			var newComment = new Post();
      		var commentContent = $("#commentContent").val();
      		var user = Parse.User.current();
      		var username = user.get("username");  

		    newComment.set("comment", commentContent);
		    newComment.set("user", user);

		    var output = "";
		    output += "<li>";
      		output += "User:"+ " "+ username
      		output += "<p> comment:" +commentContent+"</p>"
      		output += "</li>";

			newComment.save({
				success: function(){
					alert("Comment Saved")
					$("#newComment").prepend(output);
				}, 
				error: function(error){
					alert("Error:" + error.message);
				}
			})
			var commentContent = $("#commentContent").val("");
 		}
 

		
 
	</script>

	<!-- CSS -->
	<style scoped>
 	#postSubmit {
 		font-family:Times;
 		margin-left:180px;
 	}

 	#listPost {
 		border: 1px solid orange;
 		margin-top:20px;
 	}

 	.deletePost {
 		margin-top:10px;
 		margin-bottom:10px;
 		margin-left: 330px;
 	}
	
	#commentSubmit {
 		font-family:Times;
 		margin-left:180px;
 	}

 	
	</style>

</post>