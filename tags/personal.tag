<personal>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6" id="left">
                    <div id="instruction1">
                        <h3><span>{ user }</span>'s Personal Profile</h3>
                    </div>
                    <br>
                    <div id="info1">
                        <div id="showMyPic"></div>
                        <!-- <div id="showMyPic">{ myPic }</div> -->
                        <h3>Username:</h3>
                        <h4>{ user }</h4>
                        <h3>Reading Interests:</h3>
                        <h4>{ interest }</h4>
                        <h3>English Level:</h3>
                        <h4>{ engLevel }</h4>
                        <h3>Self-Intro:</h3>
                        <h4>{ selfIntro }</h4>
                        <button class="btn btn-warning" onclick={ editAll }>Edit</button>
                        <button class="btn btn-default" onclick="location.href='personalPage.html'">Go To My Personal Page</button>
                    </div>
                </div>
                <!-- edit info section -->
                <div class="col-md-6" id="right">
                    <div id="editInfo" show={ x }>
                    <!-- <div class="hidden" id="editInfo"> -->
                        <div>
                            <div id="instruction2"><h2>Update your information</h2></div>
                            <div id="info2">
                                <div class="container">
                                    <!-- self intro -->
                                    <form>
                                        <h3>Self-Introduction:</h3>
                                        <textarea id="selfContent" rows=8 cols=100 placeholder="Introduce Yourself here!"></textarea>
                                        <h4><h3>Choose your picture.</h3><input id="myPic" type="file"></h4>
                                    </form>
                                    <!-- interest -->
                                    <div id="myInterest">
                                        <h3>Reading Interest:</h3>
                                        <label><input type="checkbox" value="stem"> STEM </label>
                                        <label><input type="checkbox" value="culture"> Culture </label>
                                        <label><input type="checkbox" value="health"> Health </label>
                                        <label><input type="checkbox" value="entertainment"> Entertainment </label>
                                    </div>
                                    <!-- English level -->
                                    <div id="myLevel">
                                        <h3>English Level:</h3>
                                        <button class="btn btn-info btn-sm" id="testLevel" onclick={ testLevel }>Test your level</button>
                                        <div class="hidden" id="testEng">
                                            <div>
                                                <embed src='https://learnenglishteens.britishcouncil.org/content' width="1000" height="800"></embed>
                                            </div>
                                            <button class="btn btn-success btn-sm" id="testLevelTwo" onclick={ testLevelTwo }>Close The Test</button>
                                        </div>
                                        <h3 style="color:red">Report your level down here.</h3>
                                        <p> 1 = A1~A2 (beginner);
                                        <br>2 = A2~B1 ;
                                        <br>3 = B1~B2 ;
                                        <br>4 = B2~C1 ;
                                        <br>5 = C1~C2 (advanced)</p>
                                        <label class="radio-inline"><input type="radio" name="levelNum" value="1">1</label>
                                        <label class="radio-inline"><input type="radio" name="levelNum" value="2">2</label>
                                        <label class="radio-inline"><input type="radio" name="levelNum" value="3">3</label>
                                        <label class="radio-inline"><input type="radio" name="levelNum" value="4">4</label>
                                        <label class="radio-inline"><input type="radio" name="levelNum" value="5">5</label>
                                        <p></p>
                                    </div>
                                    <button class="btn btn-warning" onclick={ infoUpdate }>Update</button>
                                    <button class="btn btn-default" onclick={ cancelEdit }>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    this.x = false;
    var that = this;
    that.myInfo = undefined;
    var query = new Parse.Query("UserInfo");
    query.equalTo("user", Parse.User.current()).first().then(function(result) {
            that.myInfo = result;
            var username = Parse.User.current().get("username")
            that.user = username;
            that.interest = result.get("interest");
            that.engLevel = result.get("levelnum");
            that.selfIntro = result.get("selfIntro");
            var file = result.get("file");
            if (file) {
                var url = file.url();
                var img = ""
                img = "<img src='" + url + "'>";
                $("#showMyPic").html(img);
            } else {
                $("#showMyPic").html("<img src='http://placehold.it/300x400'>");
            }
            that.update();
        })
        // show or hidden editing
    this.editAll = function(event) {
        this.x = true;
    }
    this.cancelEdit = function(event) {
            this.x = false;
        }

        // show or hidden embedded test
    this.testLevel = function(event) {
        $("#testEng").removeClass("hidden")
    }
    this.testLevelTwo = function(event) {
        $("#testEng").addClass("hidden")
    }    
    
        // update function
    this.infoUpdate = function(event) {
        // may or may not update self intro
        if (document.getElementById('selfContent').value != "") {
            var introContent = $("#selfContent").val();
            that.myInfo.set("selfIntro", introContent);
        }
        // may or may not update interests
        var readingInterest = $('#myInterest input:checked').map(function() {
            return $(this).val();
        }).toArray();
        if (readingInterest.length != 0) {
            that.myInfo.set("interest", readingInterest);
        }
        // may or may not update level
        var myEngLevel = $('[name="levelNum"]:checked').val();
        var myLevelNum = parseInt(myEngLevel);
        // that.myInfo.set("level", myEngLevel);
        if(myLevelNum){
            that.myInfo.set("levelnum", myLevelNum);
        }        
        // may or may not update profile pic
        if (document.getElementById('myPic').value == "") {
            that.myInfo.save({
                success: function() {
                    console.log("info updated!");
                    $('#alert').removeClass('hidden');
                    // window.location.href = 'userprofile.html';
                },
                error: function(error) {
                    alert("Error:" + error.message);
                }
            });
        } else {
            var fileElement = $("#myPic")[0];
            var filePath = $("#myPic").val();
            var fileName = filePath.split("\\").pop()
            var file = fileElement.files[0];
            var newFile = new Parse.File(fileName, file);
            newFile.save({
                success: function() {
                    console.log("Picture updated");
                },
                error: function(error) {
                    alert("File Save Error:" + error.message)
                }
            }).then(function(file) { //save pic first and then save other info
                that.myInfo.set("file", file);
                that.myInfo.save({
                    success: function() {
                        console.log("Info updated!");
                        $('#alert').removeClass('hidden');
                        // window.location.href = 'userprofile.html';
                    },
                    error: function(error) {
                        alert("Error:" + error.message);
                    }
                })
            })
        }
    }
    </script>

    <style scoped>
    #left {
        width: 400px;
        border-radius: 5px;
        border: 1px solid rgba(0, 0, 0, .1);
        background-color: rgba(244, 241, 234, 1);
        color: black;
    }
    
    span {
        display: inline-block;
    }
    
    span:first-letter {
        text-transform: uppercase;
    }
    
    #info1 {
        /*text-align: center;*/
        padding: 15px;
        margin-top: -20px;
    }
    
    #info1 img {
        width: 75%;
        height: auto;
    }
    
    #instruction1,
    #instruction2 {
        height: 40px;
        text-align: center;
    }
    
    #instruction2 {
        background-color: rgba(104, 65, 18, .7);
        color: white;
        width: 400px;
        border-radius: 5px;
    }
    
    #instruction1 h2,
    #instruction2 h2 {
        font-weight: 300;
        font-size: 20px;
        /*position: relative;*/
        padding-top: 8px;
    }
    
    #info2 textarea {
        width: 375px;
        height: 100px;
    }
    
    #info1 h3,
    #info2 h3 {
        font-weight: 500;
        line-height: 18px;
        font-size: 18px;
    }
    
    #info1 h3 span,
    #info1 h4,
    #info2 h3 span,
    #info2 h4 {
        font-weight: 300;
        font-size: 16px;
    }
    
    #editInfo {
        margin-left: 40px;
    }
    </style>
</personal>
