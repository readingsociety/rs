<interestTag>
    <h4>Categories:</h4>
    <button class="btn btn-default" value="stem" onclick={ showArticle }>STEM</button>
    <button class="btn btn-default" value="culture" onclick={ showArticle }>Culture</button>
    <button class="btn btn-default" value="entertainment" onclick={ showArticle }>Entertainment</button>
    <button class="btn btn-default" value="health" onclick={ showArticle }>Health</button>
    <!-- JS -->
    <script>
    var that = this;
    this.showArticle = function(event) {
        var postTag = event.target.value;
        console.log(postTag);
        var query = new Parse.Query("UserPost");
        query.equalTo('interestTags', postTag).find().then(function(result) {
            riot.mount('#maincontent', 'ca', {
                articles: result
            });
        });
    };
    </script>
    <style scoped>
    :scope {
        margin-top: 80px;
    </style>
</interestTag>
