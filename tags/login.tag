<login>
    <form class="form-inline" role="form">
        <div class="form-group">
            <input type="username" class="form-control" placeholder="Username" id="username">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" id="pwd">
        </div>
        <button type="submit" class="btn btn-default" onclick={userLogIn}>Log In</button>
    </form>

    <!-- js -->

    <script>
    var user = new Parse.User();
    this.userLogIn = function(event) {
        var name = $("#username").val();
        var pass = $("#pwd").val();
        this.username.value = "";
        this.pwd.value = "";
        Parse.User.logIn(name, pass, {
            success: function(user) {
                window.location.href = 'personalPage.html';
            },
            error: function(user) {
                alert('Log in Error: Try Again.');
            }
        })
    }
    </script>


    <!-- css -->
    <style>
        .form-control {
            width: 150px;
        }
    </style>

</login>
