<loginstatus>
    <strong>Welcome, <span>{ user }</span></strong>
    <button class="btn btn-default" onclick="location.href='userprofile.html'">Profile</button>
    <button class="btn btn-default" onclick={ userLogOut }>Log out</button>
    <script>
    this.user = Parse.User.current().get('username');
    this.userLogOut = function(event) {
        Parse.User.logOut();
        window.location.href = 'index.html';
    }
    </script>
    <style scoped>
    span {
        display: inline-block;
    }
    span:first-letter {
        text-transform: uppercase;
    }
    </style>
</loginstatus>
