<learningStatus>
    <div id="learningStatus">
        <h2>Good job, <span>{ user }</span></h2>
        <h3>You have read <span>{ number }</span> articles</h3>
        <h3>and earned <span>{ time }</span> points</h3>
        <h3>Your current level is: Lv <span>{ level }</span></h3>
        <h3>A ZA A ZA Fighting!</h3>
    </div>

    <script>
    var that = this;
    this.user = Parse.User.current().get("username");
    var query = new Parse.Query('UserInfo');
    query.equalTo('user', Parse.User.current()).first().then(function(result){
    	that.time = Math.round(result.get("time"));
    	that.number = result.get("records").length;
    	that.level = result.get('levelnum');
    	that.update();
    })

    </script>
    <style scoped>
    /* scoped styles suddenly don't work, so I also copy them to the css file */
    #learningStatus h2 {
        font-size: 28px;
    }
    #learningStatus h2 span {
        font-weight: 700 bold;
        color: rgba(165, 65, 18, 1);
        display: inline-block;
    }
    #learningStatus h2 span:first-letter {
        text-transform: uppercase;
    }
    #learningStatus h3 {
        font-weight: 400;
        line-height: 8px;
        font-size: 18px;
    }
    #learningStatus h3 span {
		font-weight: bold;
        color: rgba(165, 65, 18, 1);
        font-size: 26px;
	}
    </style>
</learningStatus>
