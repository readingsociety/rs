<postcontent>
    <div class="postContent">
        <div>
            <div id="info">
                <h2>{ title }</h2>
                <h5>Author: { author }</h5>
                <h5>Posted: { time }</h5>
                <h5>Category: { postTags.toString() }</h5>
                <h5>Difficulty: { difficulty }</h5>
            </div>
            <img src="{ picLink }">
            <p></p>
            <p each={ part in content.split(/\n/) }>{ part }</p>
        </div>
        <div id="feedback">
            <h2>Feedback</h2>
            <div>
                <form role="form">
                    <div class="form-group">
                        <h3>Did you like this article?</h3>
                        <label class="radio-inline">
                            <input type="radio" name="feedback1" id="optionsRadios1" value="Yes"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="feedback1" id="optionsRadios2" value="No"> No
                        </label>
                    </div>
                    <div class="form-group">
                        <h3>How well did you understand this article? </h3>
                        <h5>( 1 = "Not at all.", 5 = "Very well.")</h5>
                        <label class="radio-inline">
                            <input type="radio" name="feedback2" id="optionsRadios1" value="1"> 1
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="feedback2" id="optionsRadios2" value="2"> 2
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="feedback2" id="optionsRadios3" value="3"> 3
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="feedback2" id="optionsRadios4" value="4"> 4
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="feedback2" id="optionsRadios5" value="5"> 5
                        </label>
                    </div>
                    <button class="btn btn-warning" onclick={ saveFeedback }>Submit</button>
                </form>
            </div>
        </div>
    </div>
    
    
    <script>
    var that = this;
    // start the time
    this.on('mount', function(){
            that.timeSpent = 0;
            that.timer = setInterval(function(){
                that.timeSpent += 1;
            }, 1000);
        });
    that.title = "Now Loading..."
    that.postId = undefined;
    that.likes = undefined;
    that.reader = undefined;
    that.readerId = undefined;
    var query = new Parse.Query('UserInfo');
    query.equalTo('user', Parse.User.current()).first().then(function(result) {
        that.reader = result;
        that.readerId = result.id;
        return Parse.Promise.as(that.reader);
    }).then(function(reader) {
        var query = new Parse.Query("UserPost");
        query.equalTo("objectId", that.opts.objectID);
        query.include('user');
        query.first().then(function(result) {
            var username = result.get('user').get("username");
            that.postId = result.id;
            that.title = result.get("title");
            that.author = username;
            that.time = moment(result.get("createdAt")).fromNow();
            that.picLink = result.get("picLink");
            that.content = result.get("content");
            that.difficulty = result.get("difficulty");
            that.postTags = result.get("interestTags");
            that.likes = result.get("likes");
            that.update();
        })
    })


    this.saveFeedback = function(event) {
        var UserPost = Parse.Object.extend('UserPost');
        var userPost = new UserPost();
        userPost.id = that.postId;
        userPost.addUnique('readBy', that.readerId);
        var valone = $('input[name=feedback1]:radio:checked').val();
        if (valone == "Yes") {
            that.likes += 1;
            userPost.set('likes', that.likes);
        }
        userPost.save();

        var valtwo = $('input[name=feedback2]:radio:checked').val();
        var confidence = Number(valtwo);
        var records = that.reader.get('records');
        var level = that.reader.get('levelnum');
        var totalTime = that.reader.get("time");
        var i = records.length - 1;
        if (i >= 0) {
            if (confidence < 3 && records[i] < 3) {
                console.log('declined');
                $('#alert').removeClass('hidden');
                $('#alert-decline').removeClass('hidden');
                level -= 1;
                if (level < 1) {
                    level = 1;
                }
            } else if (confidence > 4 && records[i] > 3) {
                console.log('improved');
                $('#alert').removeClass('hidden');
                $('#alert-improve').removeClass('hidden');
                level += 1;
                if (level > 5) {
                    level = 5;
                }
            }
        }
        clearInterval(that.timer);
        totalTime = totalTime + that.timeSpent;
        that.reader.set('time',totalTime);
        that.reader.add('records', confidence);
        that.reader.set('levelnum', level);
        that.reader.save();
        console.log('feedback received');
        $('#alert').removeClass('hidden');
        $('#alert-feedback').removeClass('hidden');
    }		
    </script>
    <style scoped>
    #info {
        /*height: 120px;*/
        width: auto;
        border-radius: 5px;
    }
    #info h2 {
        font-weight: 500;
    }
    #info h5 {
        padding-top: 8px;
        font-weight: 400;
        font-size: 13px;
        line-height: 4px;
    }
    #feedback h2 {
        font-size: 24px;
    }
    #feedback h3 {
        font-weight: 400;
        font-size: 16px;
    }
    </style>
</postcontent>
